package fit5042.tutex.calculator;

import fit5042.tutex.repository.entities.Property;
import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.Remote;



@Remote
public interface CompareProperty {
	void addProperty(Property property);
	void removeProperty(Property property);
	int bestPerRoom();
	CompareProperty create() throws CreateException, RemoteException;

}
